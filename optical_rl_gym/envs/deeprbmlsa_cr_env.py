'''
This environment attributes dynamic operation to RBMLSAEnv
'''

import numpy as np

from .deeprbmlsa_env import DeepRBMLSAEnv


class DeepRBMLSACrEnv(DeepRBMLSAEnv):

    def __init__(self, scenario, topology=None, j=1,
                 episode_length=1000,
                 load=250,
                 mean_service_holding_time=500,
                 node_request_probabilities=None,
                 seed=None,
                 k_paths=5,
                 allow_rejection=False,
                 reward_function=0 # baseline function by default
                 ):
        super().__init__(scenario=scenario, topology=topology,
                         episode_length=episode_length,
                         load=load,
                         mean_service_holding_time=mean_service_holding_time,
                         node_request_probabilities=node_request_probabilities,
                         seed=seed,
                         k_paths=k_paths,
                         allow_rejection=allow_rejection)
        self.reward_function = reward_function

    def reward_1(self, band, path_selected):
        if(self.service.accepted == False):
            return -1

        band_types = ['C', 'L', 'S', 'E']
        band_percent_usage = {'C': 0, 'L': 0, 'S': 0, 'E': 0}

        for sc_band in range(self.scenario):
            spectrum = np.array(
                self.get_available_slots(path_selected, sc_band))
            fsu_used, fsu_available = np.bincount(spectrum, minlength=2)
            band_percent_usage[band_types[sc_band]
                               ] = fsu_used / (fsu_available + fsu_used)

        reward_band_usage = 1 - band_percent_usage[band_types[band]]

        return reward_band_usage

    def reward_2(self, band):
        if(self.service.accepted == False):
            return -1
        else:
            if(band == 0):
                return 1
            if(band == 1):
                return 0.75
            if(band == 2):
                return 0.5
            if(band == 3):
                return 0.25

    def reward_3(self, band, path_selected):
        if(self.service.accepted == False):
            return -1
        return self.reward_1(band, path_selected) + self.reward_2(band)

    def reward(self, band, path_selected):
        if(self.reward_function == 0): # Baseline
            return super().reward(band, path_selected)
        elif(self.reward_function == 1): # Dynamic
            return self.reward_1(band, path_selected)
        elif(self.reward_function == 2): # Static
            return self.reward_2(band)
        elif(self.reward_function == 3): # Mixed
            return self.reward_3(band, path_selected)
